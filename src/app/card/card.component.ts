import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  shared = false;
  liked = false;
  likes = 0;

  constructor() { }

  ngOnInit(): void {
  }

  share(){
    this.shared = true;
  }

  like(){
    this.liked = true;
    if(this.likes < 5) this.likes++;
  }

}
